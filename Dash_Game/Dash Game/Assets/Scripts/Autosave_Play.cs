﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[InitializeOnLoad]
public class Autosave_Play : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    static void Autosave()
    {
        EditorApplication.playModeStateChanged += SaveOnPlay;
    }
    private static void SaveOnPlay(PlayModeStateChange state)
    {
        Debug.Log("Auto-saving...");
        if (state == PlayModeStateChange.ExitingEditMode)
        {
            Debug.Log("Auto-saving...");
            EditorSceneManager.SaveOpenScenes();
            AssetDatabase.SaveAssets();
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
