﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviour {
    private GameObject texte;
	private Rigidbody2D rigidbody2D;
    private GameObject particle;
    [Header("SFX")]
    public /*Array*/ AudioClip[] SFX;
    private AudioClip usedaudio;
    [Space]
    [Header("Floats")]
    public GameObject gameover;
    public float timeRecovery;
    private float speed = 250;
    public float dashSpeed;
    public float Speed;
    public float dashnumber;
	private Vector2 input = new Vector2();
	private Vector2 movement = new Vector2();
    [Space]
    [Header("Life Points")]
    public int hp;
    private bool hitten;
    public static bool dead;
    [Space]

    [Header("Layers")]
    public LayerMask groundLayer;

    [Space]

    public bool onGround;
    private bool jumpsynchr;
    Animator anim;
    [Space]

    [Header("Collision")]
    [HideInInspector]
    public bool isDashing;
    public float collisionRadius = 0.25f;
    public Vector2 bottomOffset;
    private Color debugCollisionColor = Color.red;

    private void Start()
    {
        texte = GameObject.Find("life");
        anim = GetComponent<Animator>();
        particle = GameObject.Find("particle effect");
        particle.SetActive(false);
        dashnumber = 0;
		rigidbody2D = GetComponent<Rigidbody2D>();
	}
	
	private void Update() {

        //if(((Input.GetKeyDown("space") || Input.GetKeyDown("joystick button 0"))))
        //particle.SetActive(true);
        if (dashnumber != 2 && !isDashing)    
            if (/*xRaw != 0 || yRaw != 0) &&  (*/(Input.GetKeyDown("space") || Input.GetKeyDown("joystick button 0")))
            {
                StartCoroutine(SetupDash());
            }
        if (!onGround && (isDashing))
            particle.SetActive(true);
        if (onGround)
        {
            anim.SetBool("OnAir", false);
            dashnumber = 0;
            if (dashnumber != 0)
            {


            }
            //if(usedaudio != SFX[1])
            //{
            //    usedaudio = SFX[0];
            //}
            TextMeshProUGUI mText = texte.GetComponent<TextMeshProUGUI>();
            mText.text = "Life Points: " + hp;
        }

                


        onGround = Physics2D.OverlapCircle((Vector2)transform.position + bottomOffset, collisionRadius, groundLayer);
        
    }
    IEnumerator SetupDash()
    {
        float xRaw = Input.GetAxisRaw("Horizontal");
        float yRaw = Input.GetAxisRaw("Vertical");
        anim.SetBool("jump", true);
        if (dashnumber == 0)
        {
            dashSpeed = Speed;
            usedaudio = SFX[0];
            yield return new WaitForSeconds(.35f);
            Sounds();
            yield return new WaitForSeconds(.20f);
            Dash(xRaw, yRaw);
        }
        if (dashnumber == 1)
        {
            dashSpeed = Speed * 0.75f;
            usedaudio = SFX[1];
            //yield return new WaitForSeconds(.10f);
            Sounds();
            //yield return new WaitForSeconds(.20f);
            Dash(xRaw, yRaw);
        }

        yield return new WaitForSeconds(0.1f);
        isDashing = true;
        dashnumber++;
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        var positions = new Vector2[] { bottomOffset };

        Gizmos.DrawWireSphere((Vector2)transform.position + bottomOffset, collisionRadius);
    }

    private void FixedUpdate() {
		movement_manager();
	}
	private void Dash(float x, float y)
    {

        anim.SetTrigger("dash");
        //Camera.main.transform.DOComplete();
        //Camera.main.transform.DOShakePosition(.2f, .5f, 14, 90, false, true);
        //hasDashed = true;

        //anim.SetTrigger("dash");
        rigidbody2D.velocity = Vector2.zero;
        Vector2 dir = new Vector2(x, y);
        rigidbody2D.AddForce(dir * dashSpeed, ForceMode2D.Impulse);


        //rigidbody2D.velocity += dir.normalized * dashSpeed;
        StartCoroutine(WaitDash());
        StartCoroutine(WaitJump());
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "projectile" && !hitten && !isDashing)
        {
            anim.SetTrigger("damage");
            //StartCoroutine(TakeDamage());
            hitten = true;
            hp = hp - 1;
            StartCoroutine(Recovery());
            if (hp == 0)
                Death();
        }
    }
    IEnumerator WaitJump()
    {
        yield return new WaitForSeconds(0.1f);
        if(onGround)
        {
            anim.SetTrigger("ground");
            particle.SetActive(false);
        }
        
    }
    //IEnumerator TakeDamage()
    //{
    //    yield return new WaitForSeconds(0.06f);
    //}
    void Death()
    {
        anim.SetTrigger("death");
        dead = true;
        StartCoroutine(WaitDeath());
    }
            void Sounds()
    {
        AudioSource yourAudioSource;
        yourAudioSource = GetComponent<AudioSource>();
        yourAudioSource.clip = (usedaudio);  
        yourAudioSource.Play();
    }
    IEnumerator Recovery()
    {
        yield return new WaitForSeconds(timeRecovery);
        hitten = false; 
    }
    IEnumerator WaitDeath()
    {
        yield return new WaitForSeconds(2f);
        gameObject.SetActive(false);
        gameover.SetActive(true);
    }
    IEnumerator WaitDash()
    {
        yield return new WaitForSeconds(0.1f);
        anim.SetBool("OnAir", true);
        if (dashnumber == 2)
        {
            anim.SetTrigger("dash");
        }

        if (dashnumber == 1)
        {
            anim.SetTrigger("onair");
        }

        yield return new WaitForEndOfFrame();
        //anim.SetBool("Dash", false);
        Sounds();
        isDashing = false;
    }
    private void movement_manager() {
        
		float horizontal = Input.GetAxis("Horizontal");
        if (horizontal == 0)
            anim.SetBool("Run", false);
        else
            anim.SetBool("Run", true);
            //float vertical = Input.GetAxis("Vertical");
            if (horizontal < 0)
            transform.eulerAngles = new Vector3(0, 0, 0); // Normal
     if(horizontal >0)
        transform.eulerAngles = new Vector3(0, 180, 0); // Flipped
        input = new Vector2(horizontal, 0);
		movement = input;
		
		rigidbody2D.velocity = new Vector2(movement.normalized.x * speed * Time.fixedDeltaTime, rigidbody2D.velocity.y);
        //if(horizontal<1)

        //if (horizontal>1)

    }
}