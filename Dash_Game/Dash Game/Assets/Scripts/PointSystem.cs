﻿using System.Collections;
using System.Collections.Generic;
using System;   
using UnityEngine;
using TMPro;

public class PointSystem : MonoBehaviour
{
    bool allinactive;
    public static int numberspawner;
    public static bool b_newwave;
    public static int wave;
    public static int points;
    private int actualTime = 0;
    private bool second;
    private GameObject g_wave;
    private GameObject g_points;
    private GameObject player;
    GameObject[] spawner;

    void Start()
    {
        wave = 1;
        g_wave = GameObject.Find("wave");
        g_points = GameObject.Find("points");
        b_newwave = false;
        spawner = GameObject.FindGameObjectsWithTag("spawner");
        player = GameObject.FindWithTag("Player");
    }

        // Update is called once per frame
        void Update()
    {
        TextMeshProUGUI mTextwave = g_wave.GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI mTextpoints = g_points.GetComponent<TextMeshProUGUI>();
        mTextwave.text = "Wave: " + wave;
        mTextpoints.text = "Points: " + points;
        if (!second)
            StartCoroutine(Timer());

        //Debug.Log(points);
    }
    public void CheckWave()
    {
        if (numberspawner == 2)
        {
            Debug.Log("number spawner" + numberspawner);
            b_newwave = true;              
            NewWave();
        }

    }
    public void NewWave()
    {
        numberspawner = 0;
        wave++;
        Debug.Log(wave);
        if(numberspawner == 2)
        {
            for (int i = 0; i < spawner.Length; i++)
            {
                GameObject select = spawner[i];
                select.GetComponent<PooledSpawner>().Spawn();
            }
        }

    }
    IEnumerator Timer()
    {
        if (PlayerController.dead = false)
        {
            second = true;
            yield return new WaitForSeconds(1f);
            points += 1 * wave;
            second = false;
        }
        else
        {
            EndoftheGame();
        }
            yield return new WaitForEndOfFrame();
    }
    void EndoftheGame()
    {

    }
}
    