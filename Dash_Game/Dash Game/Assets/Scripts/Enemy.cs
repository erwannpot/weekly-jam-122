﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Animator anim;
    public int enemyType;
    public bool BigTony;
    private bool hasshot;
    public GameObject projectile;
    public float time;

    public AudioClip[] SFX;
    private AudioClip usedaudio;

    // Start is called before the first frame update
    void Awake()
    {
        //PointSystem.points = PointSystem.points + 10;
        //time = time/PointSystem.wave;
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "projectile")
        {
            if (BigTony = true)
            {
                PointSystem.points = PointSystem.points + 10;
                anim.SetTrigger("damage");
                BigTony = false;

                usedaudio = SFX[3];
                Sounds();
            }
            else
                Death();
                usedaudio = SFX[1];
                Sounds();
        }
}
    void Death()
    {
        PointSystem.points = PointSystem.points + 10;
        anim.SetTrigger("dead");
    }
            // Update is called once per frame
            void Update()
    {
        //projectile.GetComponent<Projectile>().Spawn();
        if (!projectile.activeSelf)
            StartCoroutine(WaitSpawn());
    }
    IEnumerator WaitSpawn()
    {
        projectile.SetActive(true);
        projectile.GetComponent<Projectile>().Spawn();
        yield return new WaitForSeconds(1);

        usedaudio = SFX[2];
        Sounds();
    }

    void Sounds()
    {
        AudioSource yourAudioSource;
        yourAudioSource = GetComponent<AudioSource>();
        yourAudioSource.clip = (usedaudio);
        yourAudioSource.Play();
    }
}
