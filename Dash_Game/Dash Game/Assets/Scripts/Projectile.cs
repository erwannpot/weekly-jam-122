﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    //public GameObject go_anim;
    public float speed;
    public GameObject linkedprojectile;
    private Vector2 point;
    private GameObject player;
    private Vector2 v_player;
    private Vector2 v_shooter;
    private Vector2 direction;
    private Vector2 v_angle;
    private Vector2 newdir;
    private bool reflect;
    private Rigidbody2D rb;
    public Sprite[] sprite;
    private Animator anim;
    public Vector2 pos;

    public AudioClip[] SFX;
    private AudioClip usedaudio;

    ContactPoint2D[] contacts = new ContactPoint2D[2];
    void Start()
    {
        //int type = linkedprojectile.GetComponent<Enemy>().enemyType;
        //if (type == 1)
        //    GetComponent<SpriteRenderer>().sprite = sprite[0];
        //    if (type == 2)
        //    GetComponent<SpriteRenderer>().sprite = sprite[1];
        //if (type == 3)
        //    GetComponent<SpriteRenderer>().sprite = sprite[2];
        reflect = false;
        gameObject.SetActive(false);
        player = GameObject.FindWithTag("Player");
        rb = GetComponent<Rigidbody2D>();
        //Spawn();
        

    }

    void FixedUpdate()   
    {
        //Vector reflection
        if (!reflect)
        {
            gameObject.transform.Translate(direction * speed * Time.deltaTime);
        }
        else
        {
            v_angle = linkedprojectile.GetComponent<Ref>().v_angle;
            gameObject.transform.Translate(direction * v_angle * speed/4 * PointSystem.wave * Time.deltaTime);
        }


    }
    void Awake()
    {
        Debug.Log("sale pute");
        StartCoroutine(DestroyTime());
    }
    IEnumerator DestroyTime()
    {
        yield return new WaitForSeconds(2f);
        Spawn();
        yield return new WaitForSeconds(0.1f);
    }
    void Update()
    {

    }
        void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "wall")
        {

            Debug.Log("tu m'entends connard");
            gameObject.SetActive(false);
        }
        if(collision.gameObject.tag == "projectile")
        {
                        gameObject.SetActive(false);
        }
        bool dashed = player.GetComponent<PlayerController>().isDashing;
        if (collision.gameObject.tag == "Player")
        {
            if (!dashed)
            {
                reflect = false;
                gameObject.SetActive(false);

                usedaudio = SFX[1];
                Sounds();
            }
            else
                reflect = true;
            //go_anim.SetActive(true);
            ////calculate point of impact
            //collision.GetContacts(contacts);
            //point = contacts[0].point;
            //Debug.Log("point: " +  point);
            ////spawn animation of impact
            //go_anim.transform.position = point;
            //reflect = true;
                usedaudio = SFX[0];
                Sounds();
        }
        else if (collision.gameObject.tag == "enemy")
        {
            //go_anim.SetActive(true);
            //collision.GetContacts(contacts);
            //point = contacts[0].point;
            //go_anim.transform.position = point;
            gameObject.SetActive(false);

            usedaudio = SFX[1];
            Sounds();
        }

    }

    public void Spawn()
    {
        transform.localPosition = pos;
        //define spawn + vector to attack the player
        gameObject.SetActive(true);
        v_player = player.transform.position;
        Debug.Log(v_player);
        direction = new Vector2((v_player.x - transform.position.x), (v_player.y - transform.position.y));

    }

    void Sounds()
    {
        AudioSource yourAudioSource;
        yourAudioSource = GetComponent<AudioSource>();
        yourAudioSource.clip = (usedaudio);
        yourAudioSource.Play();
    }
}
