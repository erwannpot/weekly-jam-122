﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Ref : MonoBehaviour
{
    private Vector2 v_enemy;
    private  GameObject player;
    public Vector2 v_angle; 

	// Start is called before the first frame update
	void Start()
	{
        player = GameObject.FindWithTag("Player");
        v_enemy = transform.position;
        Vector2 v_player = player.transform.position;
        BoxCollider2D box = player.GetComponent<BoxCollider2D>();
        Vector2 size_player = box.size;
        Debug.Log(box.bounds.extents);
        v_angle = reflection(v_enemy, v_player,size_player);
	}

	// Update is called once per frame
	void Update()
	{
		
	}
	
	private Vector2 reflection(Vector2 enemy_position, Vector2 player_position, Vector2 player_size) {
		// getting vertices
		Vector2[] vertices = {
			new Vector2(player_position.x + player_size.x / 2, player_position.y + player_size.y / 2),
			new Vector2(player_position.x - player_size.x / 2, player_position.y + player_size.y / 2),
			new Vector2(player_position.x + player_size.x / 2, player_position.y - player_size.y / 2),
			new Vector2(player_position.x - player_size.x / 2, player_position.y - player_size.y / 2)
		};
		
		// finding distance between every vertex and enemy
		float[] vertex_distances = {
			Mathf.Sqrt(Mathf.Pow((enemy_position.x - vertices[0].x), 2) + Mathf.Pow((enemy_position.y - vertices[0].y), 2)),
			Mathf.Sqrt(Mathf.Pow((enemy_position.x - vertices[1].x), 2) + Mathf.Pow((enemy_position.y - vertices[1].y), 2)),
			Mathf.Sqrt(Mathf.Pow((enemy_position.x - vertices[2].x), 2) + Mathf.Pow((enemy_position.y - vertices[2].y), 2)),
			Mathf.Sqrt(Mathf.Pow((enemy_position.x - vertices[3].x), 2) + Mathf.Pow((enemy_position.y - vertices[3].y), 2))
		};
		
		// creating an array for only two of the points
		Vector2[] closest_player_side = new Vector2[2];
		
		// finding closest point
		closest_player_side[0] = vertices[GetIndexOfLowestValue(vertex_distances)];
		// finding the index of closest and setting it to 0
		vertex_distances[GetIndexOfLowestValue(vertex_distances)] = 10000000f;
		// find closest, bc I delete previous point from pool
		closest_player_side[1] = vertices[GetIndexOfLowestValue(vertex_distances)];
		vertex_distances[GetIndexOfLowestValue(vertex_distances)] = 10000000f;
		
		// finding angle of reflection and making it into degrees
		float angle = (Mathf.PI / 2 - Mathf.Atan2(enemy_position.y - player_position.y, enemy_position.x - player_position.x)) * 2;
		
		if(closest_player_side[0].x < player_position.x && closest_player_side[1].x < player_position.x) {
			angle = angle;
		} else if(closest_player_side[0].x > player_position.x && closest_player_side[1].x > player_position.x) {
			angle =  angle;
		} else if(closest_player_side[0].y < player_position.y && closest_player_side[1].y < player_position.y) {
			angle = angle + Mathf.PI / 2;
		}
        Debug.Log(angle);

        return new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
	}
	
	public int GetIndexOfLowestValue(float[] arr) {
		float value = float.PositiveInfinity;
		int index= -1;
		for(int i = 0; i < arr.Length; i++) {
			if(arr[i] < value) {
				index = i;
				value = arr[i];
			}
		}

		return index;
	}
}