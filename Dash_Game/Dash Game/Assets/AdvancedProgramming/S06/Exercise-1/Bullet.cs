﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AdvancedProgramming.S05.Exercise1
{
    public class Bullet : MonoBehaviour
    {
        public float Speed;
        public Vector3 Direction;

        private void FixedUpdate()
        {
            transform.position +=  Direction * Speed;
            //Debug.DrawLine(transform.position, transform.position + Direction, Color.red, 1f);
        }

        private void Start()
        {
            Invoke("AutoDestroy", 5f);
        }

        private void AutoDestroy()
        {
            Destroy(gameObject);
        }
    }
}