﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AdvancedProgramming.S05.Exercise1
{
    public class Spawner : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_Prefab;

        [SerializeField]
        private float m_SpawnRate;

        [SerializeField]
        private float m_RotationSpeed;

        [SerializeField]
        private float m_RotationSeed;


        private float m_LastSpawnTime;


        private void FixedUpdate()
        {
            float spawnInterval = 1f / m_SpawnRate;
            if (m_LastSpawnTime + spawnInterval < Time.time)
            {
                m_LastSpawnTime = Time.time;
                Vector3 direction = ComputeNextDirection();
                Quaternion rotation = Quaternion.LookRotation(direction, Vector3.up);
                GameObject bullet = Instantiate(m_Prefab, transform.position, rotation);
                bullet.GetComponent<Bullet>().Direction = ComputeNextDirection();
            }
        }

        private Vector3 ComputeNextDirection()
        {
            float seed = (Time.time + m_RotationSeed) * m_RotationSpeed;
            return new Vector3(Mathf.Sin(seed), 0, Mathf.Cos(seed));
        }
    }
}