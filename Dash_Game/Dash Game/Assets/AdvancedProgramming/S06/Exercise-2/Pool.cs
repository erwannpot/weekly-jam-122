﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AdvancedProgramming.S05.Exercise2
{
    public class Pool : MonoBehaviour
    {
        GameObject spawnedObject;
        [SerializeField]
        private GameObject grosTony;
        [SerializeField]
        private GameObject medium;
        [SerializeField]
        private GameObject small;
        int random;
        [SerializeField]
        private int m_PoolSize;
        GameObject points;

        [SerializeField]
        private int m_CurrentSize;

        private List<GameObject> m_Pool;

        private void Start()
        {
            m_Pool = new List<GameObject>();
            for (int i = 0; i < m_PoolSize; i++)
            {
                random = Random.Range(1, 3);
                Debug.Log(random);
                if(random ==1)
                { spawnedObject = Instantiate(grosTony); }
                if (random == 2)
                { spawnedObject = Instantiate(medium); }
                if (random == 3)
                { spawnedObject = Instantiate(small); }
                spawnedObject.SetActive(false);
                m_Pool.Add(spawnedObject);
            }
            m_CurrentSize = m_Pool.Count;
        }
        void Update()
        {

        }

        public GameObject GetAvailableObject()
        {
            GameObject pooledItem = null;
            bool keepSearching = true;
            int index = 0;
            while (pooledItem == null && keepSearching)
            {
                if (!m_Pool[index].activeSelf)
                {
                    pooledItem = m_Pool[index];
                    pooledItem.SetActive(true);
                }

                index++;

                if (index >= m_Pool.Count)
                {
                    keepSearching = false;

                    random = Random.Range(1, 3);
                    Debug.Log(random);
                    if (random == 1)
                    { spawnedObject = Instantiate(grosTony); }
                    if (random == 2)
                    { spawnedObject = Instantiate(medium); }
                    if (random == 3)
                    { spawnedObject = Instantiate(small); } 
                    spawnedObject.SetActive(false);
                    m_Pool.Add(spawnedObject);

                    pooledItem = spawnedObject;

                    m_CurrentSize = m_Pool.Count;
                }
            }

            return pooledItem;
        }
    }
}