﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public class PooledSpawner : MonoBehaviour
    {
        GameObject enemy1;
        GameObject enemy2;
        GameObject enemy3;
        [SerializeField]
        int random;
        [SerializeField]
        private GameObject grosTony;
        [SerializeField]
        private GameObject medium;
        [SerializeField]
        private GameObject small;
        GameObject points;
        //[SerializeField]
        //private Pool m_PoolManager;
        public bool samarche;
        [SerializeField]
        private float m_SpawnRate;

        [SerializeField]
        private float m_RotationSpeed;

        [SerializeField]
        private float m_RotationSeed;


        private float m_LastSpawnTime;

        void Start()
        {
            enemy1 = Instantiate(grosTony);
            grosTony.SetActive(false);
            enemy2 = Instantiate(medium);
            medium.SetActive(false);
            enemy3 = Instantiate(small);
            small.SetActive(false);
            points = GameObject.Find("point system");
            Spawn();
        }
        private void FixedUpdate()
        {
            if(!enemy1.activeSelf)
                if(!enemy2.activeSelf)
                    if(!enemy3.activeSelf)
                {
                points.GetComponent<PointSystem>().CheckWave();
                        PointSystem.numberspawner++;
                        //if (PointSystem.b_newwave)
                        //{
                        //    Spawn();
                        //}
            }
            //float spawnInterval = 1f / m_SpawnRate;
            //if (m_LastSpawnTime + spawnInterval < Time.time)
            //{
            //    m_LastSpawnTime = Time.time;
            //    Vector3 direction = ComputeNextDirection();
            //    Quaternion rotation = Quaternion.LookRotation(direction, Vector3.up);

            //    if (pooledBullet != null)
            //    {
            //        pooledBullet.transform.SetPositionAndRotation(transform.position, Quaternion.identity);
            //        PooledBullet bullet = pooledBullet.GetComponent<PooledBullet>();
            //        bullet.Direction = ComputeNextDirection();
            //        bullet.ResetPooledObject();
            //    }
            //}
        }
        public void Spawn()
        {
            Debug.Log("t fada");
            random = Random.Range(1, 3);
            //Debug.Log(random);
            if (random == 1)
            {
                enemy3.transform.SetPositionAndRotation(transform.position, Quaternion.identity);
                enemy3.SetActive(true);
            }

            if (random == 2)
            {
                enemy2.transform.SetPositionAndRotation(transform.position, Quaternion.identity);
                enemy2.SetActive(true);
            }
                
            if (random == 3)
            {
                enemy1.transform.SetPositionAndRotation(transform.position, Quaternion.identity);
                enemy1.SetActive(true);
            }

        }
        //private Vector3 ComputeNextDirection()
        //{
        //    float seed = (Time.time + m_RotationSeed) * m_RotationSpeed;
        //    return new Vector3(Mathf.Sin(seed), 0, Mathf.Cos(seed));
        //}
    }
