﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AdvancedProgramming.S05.Exercise2
{
    public class PooledBullet : MonoBehaviour
    {
        public float Speed;
        public Vector3 Direction;

        private void FixedUpdate()
        {
            transform.position += Direction * Speed;
            //Debug.DrawLine(transform.position, transform.position + Direction, Color.red, 1f);
        }

        public void ResetPooledObject()
        {
            Invoke("AutoDestroy", 3f);
        }

        private void AutoDestroy()
        {
            gameObject.SetActive(false);
        }
    }
}